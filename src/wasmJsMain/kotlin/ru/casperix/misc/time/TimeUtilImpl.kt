package ru.casperix.misc.time

import kotlinx.browser.window
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

actual fun setInterval(handler: () -> Unit, interval: Long) {
    window.setInterval({
        handler()
        null
    }, interval.toInt(), null)
}

actual fun getTimeMs(): Long {
    val j = getTimeMsRaw()
    val n = j.unsafeCast<JsNumber>()
    return n.toDouble().toLong()
}

fun getTimeMsRaw(): JsAny =    js("(Date.now())")

actual fun getTime(): Duration {
    return getTimeMs().toDuration(DurationUnit.MILLISECONDS)
}


