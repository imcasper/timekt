package ru.casperix.misc

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Uint8Array
import org.khronos.webgl.get

fun ArrayBuffer.toByteArray(): ByteArray {
	val data = Uint8Array(this)
	return ByteArray(byteLength) { index ->
		data.get(index)
	}
}

@ExperimentalUnsignedTypes
fun ArrayBuffer.toUByteArray(): UByteArray {
	val data = Uint8Array(this)
	return UByteArray(byteLength) { index ->
		data.get(index).toUByte()
	}
}