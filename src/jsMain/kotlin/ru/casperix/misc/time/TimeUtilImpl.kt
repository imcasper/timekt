package ru.casperix.misc.time

import kotlinx.browser.window
import kotlin.js.Date
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

actual fun setInterval(handler: () -> Unit, interval: Long) {
	window.setInterval(handler, interval.toInt())
}

actual fun getTimeMs(): Long {
	return Date.now().toLong()
}

actual fun getTime(): Duration {
	return Date.now().toLong().toDuration(DurationUnit.MILLISECONDS)
}
