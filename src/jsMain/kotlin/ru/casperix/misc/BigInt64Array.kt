package ru.casperix.misc

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.ArrayBufferView

/**
 * Exposes the JavaScript [BigInt64Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt64Array) to Kotlin
 */
external class BigInt64Array : ArrayBufferView {
    constructor(length: Int)
    constructor(array: BigInt64Array)
    constructor(array: Array<Long>)
    constructor(buffer: ArrayBuffer, byteOffset: Int = definedExternally, length: Int = definedExternally)

    override val buffer: ArrayBuffer
    override val byteLength: Int
    override val byteOffset: Int

//    fun at(index: Int): JsBigInt
}


//@Suppress("INVISIBLE_REFERENCE", "INVISIBLE_MEMBER")
//@kotlin.internal.InlineOnly
//public inline operator fun BigInt64Array.get(index: Int): Long = (asDynamic()[index] as JsBigInt).toLong()
//
//@Suppress("INVISIBLE_REFERENCE", "INVISIBLE_MEMBER")
//@kotlin.internal.InlineOnly
//public inline operator fun BigInt64Array.set(index: Int, value: Long) {
//    asDynamic()[index] = value.toJsBigInt()
//}