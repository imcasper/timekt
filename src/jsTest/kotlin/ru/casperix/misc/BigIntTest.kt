package ru.casperix.misc

import kotlin.test.Test

class BigIntTest {
    fun some() {
        val kotlinLong = 1L
        println(kotlinLong)

        val nativeLong = kotlinLong.toJsBigInt()
        println(nativeLong)

        val againKotlin = nativeLong.toLong()
        println(againKotlin)
    }
}