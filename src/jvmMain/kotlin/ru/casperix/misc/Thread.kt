package ru.casperix.misc

import kotlin.time.Duration
import kotlin.time.DurationUnit

fun Thread.join(duration: Duration) {
	//	Any case nano approximate to millisec...
	join(duration.toLong(DurationUnit.MILLISECONDS))
}