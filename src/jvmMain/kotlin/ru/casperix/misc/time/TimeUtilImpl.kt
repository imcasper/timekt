package ru.casperix.misc.time

import java.time.Instant
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

actual fun setInterval(handler: () -> Unit, interval: Long) {
	val timer = java.util.Timer()
	timer.scheduleAtFixedRate(0L, interval) { handler() };
}

actual fun getTimeMs(): Long {
	return Date().time
}

actual fun getTime(): Duration {
	return Instant.now().toEpochMilli().toDuration(DurationUnit.MILLISECONDS)
}
