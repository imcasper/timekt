package ru.casperix.misc.reflection

import ru.casperix.misc.property.BooleanProperty
import ru.casperix.misc.property.CustomProperty
import ru.casperix.misc.property.IntProperty
import ru.casperix.misc.property.StringProperty
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

object ReflectionUtil {

    inline fun <Type : Any, reified Value> readField(clazz: KClass<Type>, instance: Type, name: String): Value? {
        val member = clazz.members.firstOrNull { it.name == name }
        val property = member as? KProperty ?: return null
        property.isAccessible = true
        val value = property.call(instance)
        return value as? Value
    }

    inline fun <reified CustomClass : Any> instancesByClass(source: Any): List<CustomClass> {
        val result = mutableListOf<CustomClass>()
        source::class.members.forEach { member ->
            if (member is KProperty) {
                if (member.returnType.classifier == CustomClass::class) {
                    val item = member.call(source) as CustomClass
                    result += item
                }
            }
        }
        return result
    }

    fun properties(instance: Any): List<CustomProperty<*>> {
        val members = instance::class.memberProperties
        val propertyList = members.mapNotNull { it as? KMutableProperty<*> }

        return propertyList.mapNotNull { property ->
            when (property.returnType.classifier) {
                String::class -> {
                    StringProperty(property.name, {
                        property.setter.call(instance, it)
                    }, {
                        property.getter.call(instance) as String
                    })
                }

                Int::class -> {
                    IntProperty(property.name, {
                        property.setter.call(instance, it)
                    }, {
                        property.getter.call(instance) as Int
                    })
                }

                Boolean::class -> {
                    BooleanProperty(property.name, {
                        property.setter.call(instance, it)
                    }, {
                        property.getter.call(instance) as Boolean
                    })
                }

                else -> null
            }
        }
    }
}