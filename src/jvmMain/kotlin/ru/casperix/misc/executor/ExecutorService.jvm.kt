package ru.casperix.misc.executor

import ru.casperix.misc.executor.impl.WriteableTask
import java.util.concurrent.Executors
import java.util.concurrent.Future

actual fun createExecutorService(threads: Int?): ExecutorService {
    val useThreads = threads ?: Runtime.getRuntime().availableProcessors()
    return MultiThreadExecutorService(useThreads)
}

internal class FutureBasedTask<Result : Any> : WriteableTask<Result>() {
    @Volatile
    var future:Future<*>? = null

    override fun waitResult(): Result {
        val future = future ?: throw Exception("Invalid future")
        future.get()
        return super.waitResult()
    }

}

class MultiThreadExecutorService(threads: Int) : ExecutorService {
    private val executor = Executors.newFixedThreadPool(threads)
    override fun <Result : Any> execute(operation: () -> Result): Task<Result> {
        val task = FutureBasedTask<Result>()
        task.future = executor.submit {
            task.taskState = TaskState.RUNNING
            try {
                task.result = operation()
                task.taskState = TaskState.SUCCESS
            } catch (e: Throwable) {
                task.taskState = TaskState.FAIL
            }
        }
        return task
    }

    override fun close() {
        executor.shutdown()
    }

}