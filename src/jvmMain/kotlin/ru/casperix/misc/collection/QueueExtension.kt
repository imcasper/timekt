package ru.casperix.misc.collection

import java.util.*


fun <T> Queue<T>.pollAll(): List<T> {
	var list: MutableList<T>? = null

	while (true) {
		val message = poll()
		if (message != null) {
			if (list == null) list = mutableListOf()
			list += message
		} else {
			return list ?: emptyList()
		}
	}
}