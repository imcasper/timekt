package ru.casperix.misc

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import java.nio.IntBuffer
import java.nio.LongBuffer
import java.nio.ShortBuffer
import java.util.zip.Deflater
import java.util.zip.Inflater

fun DoubleBuffer.clone(): DoubleBuffer {
	val clone = ByteBuffer.allocate(capacity() * 8).order(ByteOrder.nativeOrder()).asDoubleBuffer()
	rewind()
	clone.put(this)
	rewind()
	return clone
}

fun doubleBufferOf(vararg args: Double): DoubleBuffer {
	val buffer = ByteBuffer.allocateDirect(args.size * 8).order(ByteOrder.nativeOrder()).asDoubleBuffer()
	args.forEach {
		buffer.put(it)
	}
	buffer.flip()
	return buffer
}

fun DoubleBuffer.toDirectFloatBuffer(): FloatBuffer {
	val elements = capacity()
	val buffer = DirectFloatBuffer(elements)
	(0 until elements).forEach {
		buffer.put(it, this[it].toFloat())
	}
	return buffer
}

fun DirectDoubleBuffer(elements: Int): DoubleBuffer {
	return ByteBuffer.allocateDirect(elements * 8).order(ByteOrder.nativeOrder()).asDoubleBuffer()
}

fun DirectDoubleBuffer(elements: Int, factory: (index: Int) -> Double): DoubleBuffer {
	val buffer = DirectDoubleBuffer(elements)
	(0 until elements).forEach {
		val item = factory(it)
		buffer.put(item)
	}
	buffer.flip()
	return buffer
}

fun DoubleBuffer.toDoubleArray(): DoubleArray {
	return DoubleArray(capacity()) { this[it] }
}

fun DoubleArray.toDirectDoubleBuffer(): DoubleBuffer {
	return DirectDoubleBuffer(size) { this[it] }
}

fun FloatBuffer.clone(): FloatBuffer {
	val output = ByteBuffer.allocate(capacity() * 4).order(ByteOrder.nativeOrder()).asFloatBuffer()
	rewind()
	output.put(this)
	rewind()
	return output
}

fun floatBufferOf(vararg args: Float): FloatBuffer {
	val buffer = ByteBuffer.allocateDirect(args.size * 4).order(ByteOrder.nativeOrder()).asFloatBuffer()
	args.forEach {
		buffer.put(it)
	}
	buffer.flip()
	return buffer
}

fun FloatBuffer.toDirectDoubleBuffer(): DoubleBuffer {
	val elements = capacity()
	val buffer = DirectDoubleBuffer(elements)
	(0 until elements).forEach {
		buffer.put(it, this[it].toDouble())
	}
	return buffer
}

fun DirectFloatBuffer(elements: Int): FloatBuffer {
	return ByteBuffer.allocateDirect(elements * 4).order(ByteOrder.nativeOrder()).asFloatBuffer()
}

fun DirectFloatBuffer(elements: Int, factory: (index: Int) -> Float): FloatBuffer {
	val buffer = DirectFloatBuffer(elements)
	(0 until elements).forEach {
		val item = factory(it)
		buffer.put(item)
	}
	buffer.flip()
	return buffer
}

fun FloatBuffer.toFloatArray(): FloatArray {
	return FloatArray(capacity()) { this[it] }
}


fun FloatArray.toDirectFloatBuffer(): FloatBuffer {
	return DirectFloatBuffer(size) { this[it] }
}


fun DirectByteBuffer(elements: Int): ByteBuffer {
	return ByteBuffer.allocateDirect(elements).order(ByteOrder.nativeOrder())
}

fun DirectByteBuffer(elements: Int, factory: (index: Int) -> Byte): ByteBuffer {
	val buffer = DirectByteBuffer(elements)
	(0 until elements).forEach {
		val item = factory(it)
		buffer.put(item)
	}
	buffer.flip()
	return buffer
}

fun DirectShortBuffer(elements: Int): ShortBuffer {
	return ByteBuffer.allocateDirect(elements * 2).order(ByteOrder.nativeOrder()).asShortBuffer()
}

fun DirectShortBuffer(elements: Int, factory: (index: Int) -> Short): ShortBuffer {
	val buffer = DirectShortBuffer(elements)
	(0 until elements).forEach {
		val item = factory(it)
		buffer.put(item)
	}
	buffer.flip()
	return buffer
}

fun DirectIntBuffer(elements: Int): IntBuffer {
	return ByteBuffer.allocateDirect(elements * 4).order(ByteOrder.nativeOrder()).asIntBuffer()
}

fun DirectIntBuffer(elements: Int, factory: (index: Int) -> Int): IntBuffer {
	val buffer = DirectIntBuffer(elements)
	(0 until elements).forEach {
		val item = factory(it)
		buffer.put(item)
	}
	buffer.flip()
	return buffer
}

@ExperimentalUnsignedTypes
fun UByteArray.toByteBuffer(): ByteBuffer {
	return ByteBuffer.wrap(asByteArray())
}

@ExperimentalUnsignedTypes
fun UByteArray.toDirectByteBuffer(): ByteBuffer {
	return asByteArray().toDirectByteBuffer()
}

@ExperimentalUnsignedTypes
fun ByteArray.toDirectByteBuffer(): ByteBuffer {
	val buffer = DirectByteBuffer(size)
	buffer.put(this)
	buffer.clear()
	return buffer
}

fun ByteArray.toByteBuffer(): ByteBuffer {
	return ByteBuffer.wrap(this)
}

fun ShortArray.toDirectShortBuffer(): ShortBuffer {
	return DirectShortBuffer(size) { this[it] }
}

fun IntArray.toDirectIntBuffer(): IntBuffer {
	return DirectIntBuffer(size) { this[it] }
}

fun BooleanArray.toDirectByteBufferBooleanAsByte(): ByteBuffer {
	val buffer = DirectByteBuffer(size)
	indices.forEach { buffer.put(it, if (this[it]) 1 else 0) }
	return buffer
}

fun ShortArray.toDirectByteBuffer(): ByteBuffer {
	val buffer = DirectByteBuffer(size * 2)
	val shortBuffer = buffer.asShortBuffer()
	indices.forEach { shortBuffer.put(it, this[it]) }
	return buffer
}

fun IntArray.toDirectByteBuffer(): ByteBuffer {
	val buffer = DirectByteBuffer(size * 4)
	val intBuffer = buffer.asIntBuffer()
	indices.forEach { intBuffer.put(it, this[it]) }
	return buffer
}

fun LongArray.toDirectByteBuffer(): ByteBuffer {
	val buffer = DirectByteBuffer(size * 8)
	val longBuffer = buffer.asLongBuffer()
	indices.forEach { longBuffer.put(it, this[it]) }
	return buffer
}

fun FloatArray.toDirectByteBuffer(): ByteBuffer {
	val buffer = DirectByteBuffer(size * 4)
	val floatBuffer = buffer.asFloatBuffer()
	indices.forEach { floatBuffer.put(it, this[it]) }
	return buffer
}

fun DoubleArray.toDirectByteBuffer(): ByteBuffer {
	val buffer = DirectByteBuffer(size * 8)
	val doubleBuffer = buffer.asDoubleBuffer()
	indices.forEach { doubleBuffer.put(it, this[it]) }
	return buffer
}


fun shortBufferOf(vararg args: Short): ShortBuffer {
	val buffer = ByteBuffer.allocateDirect(args.size * 2).order(ByteOrder.nativeOrder()).asShortBuffer()
	args.forEach {
		buffer.put(it)
	}
	buffer.flip()
	return buffer
}

fun intBufferOf(vararg args: Int): IntBuffer {
	val buffer = ByteBuffer.allocateDirect(args.size * 4).order(ByteOrder.nativeOrder()).asIntBuffer()
	args.forEach {
		buffer.put(it)
	}
	buffer.flip()
	return buffer
}

fun longBufferOf(vararg args: Long): LongBuffer {
	val buffer = ByteBuffer.allocateDirect(args.size * 2).order(ByteOrder.nativeOrder()).asLongBuffer()
	args.forEach {
		buffer.put(it)
	}
	buffer.flip()
	return buffer
}

fun ByteBuffer.toUByteArray():UByteArray {
	return toByteArray().asUByteArray()
}

fun ByteBuffer.toByteArray():ByteArray {
	return if (isDirect) {
		val bytes = ByteArray(capacity())
		bytes.put(this)
		bytes
	} else {
		array()
	}
}

fun ByteArray.put(buffer:ByteBuffer) {
	val length = maxOf(size, buffer.capacity())
	(0 until length).forEach { index ->
		this[index] = buffer[index]
	}
}

fun ShortArray.put(buffer:ByteBuffer) {
	val length = maxOf(size, buffer.capacity())
	val shorts = buffer.asShortBuffer()
	(0 until length).forEach { index ->
		this[index] = shorts[index]
	}
}

fun IntArray.put(buffer:ByteBuffer) {
	val length = maxOf(size, buffer.capacity())
	val ints = buffer.asIntBuffer()
	(0 until length).forEach { index ->
		this[index] = ints[index]
	}
}

fun LongArray.put(buffer:ByteBuffer) {
	val length = maxOf(size, buffer.capacity())
	val ints = buffer.asLongBuffer()
	(0 until length).forEach { index ->
		this[index] = ints[index]
	}
}

fun FloatArray.put(buffer:ByteBuffer) {
	val length = maxOf(size, buffer.capacity())
	val floats = buffer.asFloatBuffer()
	(0 until length).forEach { index ->
		this[index] = floats[index]
	}
}

fun DoubleArray.put(buffer:ByteBuffer) {
	val length = maxOf(size, buffer.capacity())
	val doubles = buffer.asDoubleBuffer()
	(0 until length).forEach { index ->
		this[index] = doubles[index]
	}
}

private var buffer = ByteArray(1000)

fun ByteArray.compress(): ByteArray {
	if (isEmpty()) return ByteArray(0)

	val d = Deflater()
	d.setInput(this)
	d.finish()
	val size = d.deflate(buffer)
	d.end()
	if (d.finished()) {
		return buffer.slice(0 until size).toByteArray()
	}
	buffer = ByteArray(buffer.size * 2)
	return compress()
}

fun ByteArray.decompress(): ByteArray {
	if (isEmpty()) return ByteArray(0)

	val d = Inflater()
	d.setInput(this)
	val size = d.inflate(buffer)
	d.end()
	if (d.finished()) {
		return buffer.slice(0 until size).toByteArray()
	}
	buffer = ByteArray(buffer.size * 2)
	return decompress()
}
