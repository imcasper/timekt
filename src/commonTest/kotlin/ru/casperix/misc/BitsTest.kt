package ru.casperix.misc

import ru.casperix.misc.getBits
import ru.casperix.misc.pow
import ru.casperix.misc.setBits
import kotlin.test.Test
import kotlin.test.assertEquals

class BitsTest {
	fun sliceByte(byte:Int):Int {
		return (byte shl 24) shr 24
	}

	@Test
	fun testMask() {
		assertEquals(1, 0.setBits( 0, 1, -1))
		assertEquals(0, 0.setBits( 0, 1, 0))
		assertEquals(1, 0.setBits( 0, 1, 1))
		assertEquals(0, 0.setBits( 0, 1, 2))
		assertEquals(1, 0.setBits( 0, 1, 3))

		assertEquals(0, 0.setBits( 0, 8, 0))
		assertEquals(127, 0.setBits( 0, 8, 127))
		assertEquals(128, 0.setBits( 0, 8, -128))
		assertEquals(128, 0.setBits( 0, 8, 128))
		assertEquals(255, 0.setBits( 0, 8, -1))
		assertEquals(255, 0.setBits( 0, 8, 255))

		assertEquals(255 shl 3, 0.setBits(3, 8, 255))
		assertEquals(255 shl 8, 0.setBits( 8, 8, 255))
	}

	@Test
	fun testGetBits() {
		val bits1 = Int.MAX_VALUE.getBits( 31, 1)
		assertEquals(0, bits1, "Error for sign bit")

		val bits2 = Int.MIN_VALUE.getBits( 31, 1)
		assertEquals(1, bits2, "Error for sign bit")


		for (index in 0..30) {
			val bitsFromMax = Int.MAX_VALUE.getBits( index, 1)
			assertEquals(1, bitsFromMax, "Error for bit $index")
		}

		for (index in 0..30) {
			val bitFromMin = 0.getBits( index, 1)
			assertEquals(0, bitFromMin, "Error for bit $index")
		}
	}

	@Test
	fun testSetBits() {
		for (bitAmount in 1..31) {
			testWith(bitAmount, Int.MIN_VALUE)
			testWith(bitAmount, Int.MAX_VALUE)
			testWith(bitAmount, 0)
			testWith(bitAmount, 111)
		}
	}

	private fun testWith(bitAmount: Int, sourceContainer:Int) {
		val bitsExpected = 2.pow(bitAmount) - 1

		for (bitOffset in 0..(31 - bitAmount)) {

			val container = sourceContainer.setBits( bitOffset, bitAmount, bitsExpected)
			val bitsActual = container.getBits( bitOffset, bitAmount)

			assertEquals(bitsExpected, bitsActual, "Error with bitOffset: $bitOffset; bitAmount: $bitAmount; source: $sourceContainer")
		}
	}
}