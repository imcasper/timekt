package ru.casperix.misc

import ru.casperix.misc.toPrecision
import ru.casperix.misc.toString
import kotlin.test.Test
import kotlin.test.assertEquals

class DoubleFormatterTest {
	@Test
	fun toFixed() {
		assertEquals("-123", (-123).toString(3, '#'))
		assertEquals("-12", (-12).toString(3, '#'))
		assertEquals("#-1", (-1).toString(3, '#'))
		assertEquals("##0", (0).toString(3, '#'))
		assertEquals("##1", (1).toString(3, '#'))
		assertEquals("#12", (12).toString(3, '#'))
		assertEquals("123", (123).toString(3, '#'))
		assertEquals("1234", (1234).toString(3, '#'))
	}

	@Test
	fun toPrecision() {
		assertEquals("0.0", (-0.01).toPrecision(1))
		assertEquals("-0.01", (-0.01).toPrecision(2))

		assertEquals("0.0", 0.0.toPrecision(1))
		assertEquals("0.00", 0.0.toPrecision(2))
		assertEquals("12.340", 12.34.toPrecision(3))
		assertEquals("12.345", 12.345.toPrecision(3))
		assertEquals("12.345", 12.3454.toPrecision(3))
		assertEquals("12.346", 12.3456.toPrecision(3))
		assertEquals("-12.340", (-12.34).toPrecision(3))
		assertEquals("-12.345", (-12.345).toPrecision(3))
		assertEquals("-12.345", (-12.3454).toPrecision(3))
		assertEquals("-12.346", (-12.3456).toPrecision(3))
	}

}