package ru.casperix.misc

import ru.casperix.misc.TableFormatter.format
import kotlin.test.Test

class TableTest {
    @Test
    fun simple() {
        val table = Table(
            "some", listOf(
                TableColumn(
                    16, "name", listOf(
                        "a",
                        "b",
                        "c",
                    )
                ),
                TableColumn(
                    8, "value", listOf(
                        1.toString(),
                        2.toString(),
                        3.toString(),
                    )
                ),
            )
        )
        println(table.format())
    }
}