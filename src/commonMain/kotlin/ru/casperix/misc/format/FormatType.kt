package ru.casperix.misc.format

enum class FormatType {
    DETAIL,
    NORMAL,
    SHORT,
}