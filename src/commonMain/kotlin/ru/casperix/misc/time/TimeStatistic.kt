package ru.casperix.misc.time

import ru.casperix.misc.compute
import kotlin.time.Duration


object TimeStatistic {
    private var accumulator = mutableMapOf<String, Duration>()
    private val frames = mutableListOf<Map<String, Duration>>()
    private val frameCount = 10

    fun nextFrame() {
        frames.add(accumulator)
        if (frames.size > frameCount) frames.removeFirst()

        accumulator = mutableMapOf()
    }

    fun statistic(): Map<String, Duration> {
        val summary = mutableMapOf<String, Duration>()
        frames.forEach { frame ->
            frame.forEach { (name, time) ->
                summary.compute(name) { _, last -> (last ?: Duration.ZERO) + time / frameCount }
            }
        }
        return summary
    }

    fun append(name: String, time: Duration) {
        accumulator.compute(name) { _, last -> (last ?: Duration.ZERO) + time }
    }

    fun measureTime(name: String, function: () -> Unit) {
        val time = kotlin.time.measureTime {
            function()
        }
        append(name, time)
    }
}