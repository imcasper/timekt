package ru.casperix.misc.time

/**
 * 	You call update in loop.
 *
 * 	TImer calculate time to call onEvent
 *
 * 	@param delay -- in milliseconds before first call
 * 	@param interval -- in milliseconds between calls
 */
class ManualTimer(val delay: Double, val interval: Double, val maxUpdateTimeMs:Int = 100, val onEvent: () -> Unit) {
	private var accumulatedTime = interval - delay

	fun update(time: Double) {
		accumulatedTime += time
		val startTimeMs = getTimeMs()
		while (accumulatedTime >= interval) {
			accumulatedTime -= interval
			onEvent()
			val nextTimeMs = getTimeMs()
			if (startTimeMs + maxUpdateTimeMs < nextTimeMs) {
				//overtime
				accumulatedTime = 0.0
				return
			}
		}
	}
}