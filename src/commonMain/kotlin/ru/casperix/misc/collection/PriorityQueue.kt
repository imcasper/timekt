package ru.casperix.misc.collection


class PriorityQueue<E>(val comparator: Comparator<in E>) : AbstractMutableCollection<E>() {
	private val values = mutableListOf<E>()

	override val size: Int; get() = values.size

	override fun isEmpty(): Boolean {
		return values.isEmpty()
	}

	override fun clear() {
		values.clear()
	}

	override fun contains(element: E): Boolean {
		return values.contains(element)
	}

	override fun containsAll(elements: Collection<E>): Boolean {
		return values.containsAll(elements)
	}

	override fun iterator(): MutableIterator<E> {
		return values.iterator()
	}
	
	override fun add(element: E): Boolean {
		val index: Int = values.size
		values.add(element)
		siftUp(index, element)

		return true
	}

	override fun remove(element: E): Boolean {
		val index = values.indexOf(element)
		if (index >= 0) {
			removeAt(index) != null
			return true
		}
		return false
	}

	fun removeAt(i: Int): E? {
		val s = size - 1
		if (s == i) {
			values.removeAt(i)
		} else {
			val moved: E = values[s]
			values.removeAt(s)
			siftDown(i, moved)
			if (values[i] === moved) {
				siftUp(i, moved)
				if (values[i] !== moved) {
					return moved
				}
			}
		}
		return null
	}

	fun peek(): E {
		return values[0]
	}

	fun poll(): E {
		val result = peek()
		if (result != null) {
			val n: Int = values.size - 1
			val last: E = values.removeAt(n)
			if (n > 0) {
				siftDownUsingComparator(0, last, values, n, comparator)
			}
		}
		return result
	}

	private fun siftUp(k: Int, x: E) {
		siftUpUsingComparator(k, x, values, comparator)
	}

	private fun siftDown(k: Int, x: E) {
		siftDownUsingComparator(k, x, values, size, comparator)
	}

	companion object {

		fun <Custom : Comparable<Custom>> create(): PriorityQueue<Custom> {
			return PriorityQueue<Custom>(Comparator { a, b -> a.compareTo(b) })
		}

		fun <Custom> create(comparator: Comparator<Custom>): PriorityQueue<Custom> {
			return PriorityQueue<Custom>(comparator)
		}

		private fun <T> siftDownUsingComparator(position: Int, element: T, queue: MutableList<T>, n: Int, cmp: Comparator<in T>) {
			var index = position
			var child: Int
			val half = n ushr 1
			while (index < half) {
				child = (index shl 1) + 1
				var c = queue[child]
				val right = child + 1
				if (right < n && cmp.compare(c, queue[right]) > 0) {
					child = right
					c = queue[right]
				}
				if (cmp.compare(element, c) <= 0) {
					break
				}
				queue[index] = c
				index = child
			}
			queue[index] = element
		}

		private fun <T> siftUpUsingComparator(position: Int, element: T, queue: MutableList<T>, cmp: Comparator<in T>) {
			var index = position
			while (true) {
				if (index > 0) {
					val parent = index - 1 ushr 1
					val some: T = queue[parent]
					if (cmp.compare(element, some) < 0) {
						queue[index] = some
						index = parent
						continue
					}
				}
				queue[index] = element
				return
			}
		}
	}
}