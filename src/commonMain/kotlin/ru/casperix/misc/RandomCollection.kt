package ru.casperix.misc

import kotlin.random.Random
import kotlin.random.nextInt

fun <T> Collection<Pair<T, Double>>.randomWeightedOrNull(random: Random): T? {
    return randomItem(random, this, true)
}

fun <T> Collection<Pair<T, Double>>.randomWeighted(random: Random): T {
    return randomItem(random, this, false)!!
}

fun <T> Random.nextWeightedOrNull(weightMap: Map<T, Double>, allowZero: Boolean = true): T? {
    return randomItem(this, weightMap.map { Pair(it.key, it.value) }, allowZero)
}

fun <T> Random.nextWeightedOrNull(weightMap: Collection<Pair<T, Double>>, allowZero: Boolean = true): T? {
    return randomItem(this, weightMap, allowZero)
}

fun <T> Random.nextWeighted(weightMap: Collection<Pair<T, Double>>): T {
    if (weightMap.isEmpty()) {
        throw Exception("Collection must be non empty")
    }

    return nextWeightedOrNull(weightMap, false)!!
}

fun <T> randomItem(random: Random, items: Collection<Pair<T, Double>>, allowZero: Boolean = true): T? {
    if (items.isEmpty()) return null

    items.forEach { (_, chance) ->
        if (!allowZero && chance == 0.0) {
            throw Error("Chance must be non zero")
        }
        if (chance < 0.0) throw Error("Chance must be non negative")
        if (!chance.isFinite()) throw Error("Chance must be finite value")
    }


    if (items.size == 1) return items.first().first


    val summaryChance = items.collapse(0.0) { pair, nextChance ->
        nextChance + pair.second
    }

    if (summaryChance <= 0.0) return null

    val chance = random.nextDouble(summaryChance)
    var accumulated = 0.0
    items.forEach {
        accumulated += it.second
        if (accumulated >= chance) {
            return it.first
        }
    }
    return items.last().first
}

private val defaultStringSource = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

fun Random.nextString(lengthRange: IntRange = 4..20, source: String = defaultStringSource): String {
    if (lengthRange.isEmpty() || source.isEmpty()) return ""

    val length = nextInt(lengthRange)
    if (length == 0) return ""

    val self = this
    return buildString(length) {
        repeat(length) {
            append(source.random(self))
        }
    }
}