package ru.casperix.misc


fun FloatArray.toDoubleArray(): DoubleArray {
	val elements = size
	val target = DoubleArray(elements)
	(0 until elements).forEach {
		target.set(it, this[it].toDouble())
	}
	return target
}

fun DoubleArray.toFloatArray(): FloatArray {
	val elements = size
	val target = FloatArray(elements)
	(0 until elements).forEach {
		target.set(it, this[it].toFloat())
	}
	return target
}

fun IntArray.toFloatArray(): FloatArray {
	val elements = size
	val target = FloatArray(elements)
	(0 until elements).forEach {
		target.set(it, this[it].toFloat())
	}
	return target
}

fun IntArray.toDoubleArray(): DoubleArray {
	val elements = size
	val target = DoubleArray(elements)
	(0 until elements).forEach {
		target.set(it, this[it].toDouble())
	}
	return target
}

fun FloatArray.clone(): FloatArray {
	return FloatArray(size) { this[it] }
}

fun DoubleArray.clone(): DoubleArray {
	return DoubleArray(size) { this[it] }
}