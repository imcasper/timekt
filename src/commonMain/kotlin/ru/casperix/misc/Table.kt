package ru.casperix.misc

import kotlin.math.max

data class Table(val name:String, val columns: List<TableColumn>)
data class TableColumn(val width: Int, val title: String, val values: List<String>)

object TableFormatter {

    fun Table.format(): String {
        val height = getTableHeight(this)

        val lines = mutableListOf<String>()

        lines += drawTableDummy(this, "-+-")
        lines += drawTableName(this, " | ")
        lines += drawTableDummy(this, "-+-")
        lines += drawTableTitle(this, " | ")
        lines += drawTableDummy(this, "-+-")
        (0 until height).map { line ->
            lines += drawTableRow(this, " | ", line)
        }
        lines += drawTableDummy(this, "-+-")
        return lines.joinToString("\n")
    }

    private fun getTableHeight(table: Table): Int {
        var height = 0
        table.columns.forEach {
            height = max(height, it.values.size)
        }
        return height
    }

    private fun drawTableName(table: Table, border: String): String {
        val width = table.columns.fold(0) { acc, column->acc + column.width } + (table.columns.size - 1) * border.length

        return fixedString(table.name, width)
    }

    private fun drawTableTitle(table: Table, border: String): String {
        return table.columns.map {
            fixedString(it.title, it.width)
        }.joinToString(border)
    }

    private fun drawTableDummy(table: Table, border: String): String {
        return table.columns.map {
            fixedString("", it.width, '-')
        }.joinToString(border)
    }

    private fun drawTableRow(table: Table, border: String, line: Int): String {
        return table.columns.map {
            fixedString(it.values.getOrElse(line) { "-" }, it.width)
        }.joinToString(border)
    }

    private fun fixedString(value: String, size: Int, fill: Char = ' '): String {
        if (value.length >= size) return value.substring(0, size)
        var result = value
        while (result.length < size) {
            result += fill
        }
        return result
    }

}