package ru.casperix.misc.executor.impl

import ru.casperix.misc.executor.ExecutorService
import ru.casperix.misc.executor.Task
import ru.casperix.misc.executor.TaskState


class SimulatorOfExecutorService : ExecutorService {

    override fun <Result : Any> execute(operation: () -> Result): Task<Result> {
        val task = WriteableTask<Result>()
        try {
            task.result = operation()
            task.taskState = TaskState.SUCCESS
        } catch (e: Throwable) {
            task.taskState = TaskState.FAIL
        }
        return task
    }

    override fun close() {

    }

}

