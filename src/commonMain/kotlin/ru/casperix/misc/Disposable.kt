package ru.casperix.misc

interface Disposable {
	fun dispose()
}

fun <T> getDisposable(item: T, onDispose: (T) -> Unit): Disposable {
	return object : Disposable {
		override fun dispose() {
			onDispose(item)
		}
	}
}

/**
 * You can dispose every item once time
 *
 * For example, collect signal slot, and drop it all
 */
fun <Custom : Disposable> MutableCollection<Custom>.disposeAll() {
	forEach {
		it.dispose()
	}
	clear()
}

fun <Custom : Disposable> MutableCollection<Custom>.disposeAll(condition: (Custom) -> Boolean) {
	removeAll {
		if (condition(it)) {
			it.dispose()
			true
		} else {
			false
		}
	}
}

fun mutableDisposableListOf(vararg disposable: Disposable): MutableList<Disposable> {
	return mutableListOf(*disposable)
}

open class DisposableHolder : Disposable {
	var isDisposed = false; private set

	val components = mutableListOf<Disposable>()

	override fun dispose() {
		isDisposed = true
		components.disposeAll()
	}

	inline fun <reified T> getComponent(): T? {
		return components.filterIsInstance<T>().firstOrNull()
	}
}

