package ru.casperix.misc

import kotlin.random.Random

fun <Some> List<Some>.splitByChunk(maxChunkSize: Int): List<List<Some>> {
	if (maxChunkSize <= 0) throw Error("chnuk size must be positive number (now: $maxChunkSize")

	val summarySize = size
	return (0 until summarySize step maxChunkSize).map {
		sliceSafe(it until it + maxChunkSize)
	}
}

fun <T> List<T>.sliceSafe(indices: IntRange): List<T> {
	if (isEmpty()) return listOf()
	if (indices.first >= size) return listOf()
	if (indices.last < 0) return listOf()

	val first = indices.first.clamp(0, size)
	val last = indices.last.clamp(0, size)

	return slice(first..last)
}

fun String.substringOrEmpty(start: Int, end: Int): String {
	return substringOrEmpty(start until end)
}

fun String.substringOrEmpty(indices: IntRange): String {
	if (isEmpty()) return ""
	if (indices.first >= length) return ""
	if (indices.last < 0) return ""

	val first = indices.first.clamp(0, length)
	val last = indices.last.clamp(0, length)

	return slice(first..last)
}


fun String.sliceSafe(indices: IntRange): String {
	if (isEmpty()) return ""
	if (indices.first >= length) return ""
	if (indices.last < 0) return ""

	val first = indices.first.clamp(0, length)
	val last = indices.last.clamp(0, length)

	return slice(first..last)
}

fun <T> Iterable<T>.contains(condition: (T) -> Boolean): Boolean {
	return firstOrNull { condition(it) } != null
}

fun <T, Result> Iterable<T>.collapse(initial: Result, operation: (T, Result) -> Result): Result {
	var next = initial
	forEach {
		next = operation(it, next)
	}
	return next
}

fun <T> Iterable<T>.linearSearchInfo(weigher: (T) -> Double?): Pair<Double, T>? {
	var bestRoute: T? = null
	var bestWeight = Double.NEGATIVE_INFINITY

	forEach {
		val weight = weigher(it)
		if (weight != null && bestWeight < weight) {
			bestWeight = weight
			bestRoute = it
		}
	}
	val route = bestRoute ?: return null
	return Pair(bestWeight, route)
}

fun <T> Iterable<T>.linearSearch(weigher: (T) -> Double?): T? {
	return linearSearchInfo(weigher)?.second
}

fun <T> Iterable<T>.linearSearchWeight(weigher: (T) -> Double?): Double? {
	return linearSearchInfo(weigher)?.first
}

fun <V> List<V>.forEachExist(operation: (V) -> Unit) {
	var i = -1
	while (++i < size) {
		operation(this[i])
	}
}

fun <V> List<V>.forEachExistInvert(operation: (V) -> Unit) {
	var i = size - 1
	while (i in 0 until size) {
		operation(this[i])
		i--
	}
}

fun <S, K, V> Iterable<S>.mapping(creator: (S) -> Pair<K, V>?): Map<K, V> {
	return mapping(creator, null)
}

fun <S, K, V> Iterable<S>.mapping(creator: (S) -> Pair<K, V>?, merger: ((K, V, V) -> V?)?): Map<K, V> {
	val map = mutableMapOf<K, V>()
	forEach {
		val pair = creator(it)
		if (pair != null) {
			val (key, value) = pair
			if (merger != null) {
				val lastValue = map[key]
				if (lastValue != null) {
					val next = merger(key, lastValue, value)
					if (next == null) {
						map.remove(key)
					} else {
						map[key] = next
					}
				} else {
					map[key] = value
				}
			} else {
				map[key] = value
			}
		}
	}
	return map
}

inline fun <reified T> asListOrNull(source: Any?): List<T>? {
	if (source !is List<*>) return null
	val first = source.firstOrNull() ?: return emptyList()
	if (first is T) {
		return source as List<T>
	}
	return null
}

inline fun <reified T> asList(source: Any?): List<T> {
	return asListOrNull(source)!!
}

fun <Item> Random.nextItem(items: List<Item>): Item? {
	if (items.isEmpty()) return null
	if (items.size == 1) return items[0]
	return items[nextInt(0, items.size)]
}

/**
 * Copy from java (Map.compute)
 */
fun <K, V> MutableMap<K, V>.compute(
	key: K,
	remappingFunction: (K, V?)->V?
): V? {
	val oldValue = get(key)
	val newValue = remappingFunction(key, oldValue)
	return if (newValue == null) {
		// delete mapping
		if (oldValue != null || containsKey(key)) {
			// something to remove
			remove(key)
			null
		} else {
			// nothing to do. Leave things as they were.
			null
		}
	} else {
		// add or replace old mapping
		put(key, newValue)
		newValue
	}
}

fun <K, V> MutableMap<K, V>.putIfAbsentOrElse(key: K, value: V, operation: () -> Unit) {
	if (containsKey(key)) {
		operation()
	} else {
		put(key, value)
	}
}