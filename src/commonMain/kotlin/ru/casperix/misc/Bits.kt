package ru.casperix.misc

/**
 * 	NOT slice overhead bites
 */
private const val MAX_BIT_AMOUNT = 32

fun Int.getBit( bitOffset: Int): Boolean {
	return getBits( bitOffset, 1) == 1
}

fun Int.setBit( bitOffset: Int, value: Boolean): Int {
	return setBits( bitOffset, 1, if (value) 1 else 0)
}

fun Int.getBits( bitOffset: Int, bitAmount: Int): Int {
	val offset = MAX_BIT_AMOUNT - bitAmount
	return this shl (offset - bitOffset) ushr offset
}

fun Int.setBits( bitOffset: Int, bitAmount: Int, bits: Int): Int {
	val leftOffset = (MAX_BIT_AMOUNT + bitOffset)
	val rightOffset = (bitAmount + bitOffset)
	val left = (this shl leftOffset) ushr leftOffset
	val right = (this ushr rightOffset) shl rightOffset

	val bitsCleaned = bits shl (MAX_BIT_AMOUNT - bitAmount)
	val bitsWithOffset = bitsCleaned ushr (MAX_BIT_AMOUNT - bitAmount - bitOffset)

	return left or bitsWithOffset or right
}
