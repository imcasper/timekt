package ru.casperix.misc

import kotlin.math.ceil
import kotlin.math.roundToInt

fun Double.ceilToInt():Int {
	if (this < 0.0) {
		return (this - 0.5).roundToInt()
	}
	return ceil(this).toInt()
}


fun Float.ceilToInt():Int {
	if (this < 0.0f) {
		return (this - 0.5f).roundToInt()
	}
	return ceil(this).toInt()
}

fun min(vararg values:Int):Int {
	return values.reduce { acc, next -> kotlin.math.min(acc, next) }
}

fun max(vararg values:Int):Int {
	return values.reduce { acc, next -> kotlin.math.max(acc, next) }
}

fun min(vararg values:Long):Long {
	return values.reduce { acc, next -> kotlin.math.min(acc, next) }
}

fun max(vararg values:Long):Long {
	return values.reduce { acc, next -> kotlin.math.max(acc, next) }
}

fun max(vararg values:Float):Float {
	return values.reduce { acc, next -> kotlin.math.max(acc, next) }
}

fun min(vararg values:Float):Float {
	return values.reduce { acc, next -> kotlin.math.min(acc, next) }
}

fun max(vararg values:Double):Double {
	return values.reduce { acc, next -> kotlin.math.max(acc, next) }
}

fun min(vararg values:Double):Double {
	return values.reduce { acc, next -> kotlin.math.min(acc, next) }
}

fun Int.pow(x: Int): Int = powInt(this, x)
fun Long.pow(x: Int): Long = powLong(this, x)
fun Int.clamp(min: Int, max: Int): Int = clampInt(this, min, max)
fun Long.clamp(min: Long, max: Long): Long = clampLong(this, min, max)
fun Float.clamp(min: Float, max: Float): Float = clampFloat(this, min, max)
fun Double.clamp(min: Double, max: Double): Double = clampDouble(this, min, max)

fun powInt(b: Int, e: Int): Int {
//	return b.toDouble().pow(e).roundToInt()
	if (e < 0) {
		return 1 / powInt(b, -e)
	}
	var base = b
	var exp = e
	var result = 1
	while (true) {
		if (exp and 1 == 1) {
			result *= base
		}
		exp = exp shr 1
		if (exp == 0) {
			break
		}
		base *= base
	}

	return result
}

fun powLong(b: Long, e: Int): Long {
//	return b.toDouble().pow(e).roundToInt()
	if (e < 0) {
		return 1L / powLong(b, -e)
	}
	var base = b
	var exp = e.toLong()
	var result = 1L
	while (true) {
		if (exp and 1 == 1L) {
			result *= base
		}
		exp = exp shr 1
		if (exp == 0L) {
			break
		}
		base *= base
	}

	return result
}

fun clampInt(value: Int, min: Int, max: Int): Int {
	if (max <= min) throw Error("Invalid interval $min - $max")

	if (value < min) return min;
	if (value >= max) return max - 1;
	return value
}

fun clampLong(value: Long, min: Long, max: Long): Long {
	if (max <= min) throw Error("Invalid interval $min - $max")

	if (value < min) return min;
	if (value >= max) return max - 1;
	return value
}

fun clampFloat(value: Float, min: Float, max: Float): Float {
	if (max < min) throw Error("Invalid interval $min - $max")

	if (value < min) return min;
	if (value >= max) return max;
	return value
}

fun clampDouble(value: Double, min: Double, max: Double): Double {
	if (max < min) throw Error("Invalid interval $min - $max")

	if (value < min) return min;
	if (value >= max) return max;
	return value
}

@Deprecated(message = "Use Int.mod")
fun Int.normal(dest: Int): Int {
	val result = this % dest
	if (dest > 0) {
		if (result < 0) {
			return result + dest
		} else {
			return result
		}
	} else {
		if (result > 0) {
			return result + dest
		} else {
			return result
		}
	}
}