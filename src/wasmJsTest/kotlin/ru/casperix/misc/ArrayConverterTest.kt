package ru.casperix.misc

import org.khronos.webgl.get
import kotlin.test.Test
import kotlin.test.assertEquals

class ArrayConverterTest {
    @Test
    fun testBytes() {
        val originalList = byteArrayOf(0, 1, 2, 3, 4, 5, 6, 7)

        originalList.toJsInt8Array().let { target ->
            originalList.forEachIndexed { index, originalItem ->
                val targetItem = target[index]
                assertEquals(originalItem, targetItem)
            }
        }
        originalList.toJsUInt8Array().let { target ->
            originalList.forEachIndexed { index, originalItem ->
                val targetItem = target[index]
                assertEquals(originalItem, targetItem)
            }
        }
    }
    
    @Test
    fun testShorts() {
        val originalList = shortArrayOf(0, 1, 2, 3, 4, 5, 6, 7)

        originalList.toJsInt16Array().let { target ->
            originalList.forEachIndexed { index, originalItem ->
                val targetItem = target[index]
                assertEquals(originalItem, targetItem)
            }
        }
        originalList.toJsUInt16Array().let { target ->
            originalList.forEachIndexed { index, originalItem ->
                val targetItem = target[index]
                assertEquals(originalItem, targetItem)
            }
        }
    }
    
    @Test
    fun testInts() {
        val originalList = intArrayOf(0, 1, 2, 3, 4, 5, 6, 7)

        originalList.toJsInt32Array().let { target ->
            originalList.forEachIndexed { index, originalItem ->
                val targetItem = target[index]
                assertEquals(originalItem, targetItem)
            }
        }
        originalList.toJsUint32Array().let { target ->
            originalList.forEachIndexed { index, originalItem ->
                val targetItem = target[index]
                assertEquals(originalItem, targetItem)
            }
        }
    }

//    @Test
//    fun testLongs() {
//        val originalList = longArrayOf(0, 1, 2, 3, 4, 5, 6, 7)
//
//        originalList.toJsBigInt64Array().let { target ->
//            originalList.forEachIndexed { index, originalItem ->
//                val targetItem = target[index]
//                println(targetItem)
//                assertEquals(originalItem, targetItem)
//            }
//        }
//    }

    @Test
    fun testFloats() {
        val originalList = floatArrayOf(0f, 1f, 2f, 3f, 4f, 5f, 6f, 7f)

        originalList.toJsFloat32Array().let { target ->
            originalList.forEachIndexed { index, originalItem ->
                val targetItem = target[index]
                assertEquals(originalItem, targetItem)
            }
        }
    }
    
    @Test
    fun testDoubles() {
        val originalList = doubleArrayOf(0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0)

        originalList.toJsFloat64Array().let { target ->
            originalList.forEachIndexed { index, originalItem ->
                val targetItem = target[index]
                assertEquals(originalItem, targetItem)
            }
        }
    }
}

