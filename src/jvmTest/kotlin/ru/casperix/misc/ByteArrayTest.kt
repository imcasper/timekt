package ru.casperix.misc

import ru.casperix.misc.toByteArray
import ru.casperix.misc.toByteBuffer
import ru.casperix.misc.toUByteArray
import kotlin.test.Test
import kotlin.test.assertEquals

@ExperimentalUnsignedTypes
class ByteArrayTest {
    @Test
    fun uByteConverts() {
        val expected = ubyteArrayOf(0u, 1u, 254u, 255u)

        val actual = expected.toByteBuffer().toUByteArray()

        assertEquals(expected, actual)
    }

    @Test
    fun byteConverts() {
        val expected = byteArrayOf(0, 1, -2, -1)

        val actual = expected.toByteBuffer().toByteArray()

        assertEquals(expected, actual)
    }
}