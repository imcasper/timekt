package ru.casperix.misc.executor

import ru.casperix.misc.executor.impl.SimulatorOfExecutorService
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ExecutorServiceTest {

    @Test
    fun twoProcessTest() {
        val processors = Runtime.getRuntime().availableProcessors()

        listOf(
            Pair(processors, createExecutorService(processors)),
            Pair(1, SimulatorOfExecutorService()),
        ).forEach { (threads, service) ->
            val startMs = System.currentTimeMillis()

            val task1 = service.execute {
                Thread.sleep(100L)
                1
            }

            val taskBad = service.execute {
                Thread.sleep(100L)
                throw Exception("Task trouble")
            }

            val task2 = service.execute {
                Thread.sleep(100L)
                2
            }

            val result1 = task1.waitResult()
            val result2 = task2.waitResult()

            val mustSuccess1 = task1.getState()
            val mustSuccess2 = task2.getState()
            val mustFail = taskBad.getState()


            val durationMs = System.currentTimeMillis() - startMs

            assertEquals(result1, 1)
            assertEquals(result2, 2)

            assertEquals(mustSuccess1, TaskState.SUCCESS)
            assertEquals(mustSuccess2, TaskState.SUCCESS)
            assertEquals(mustFail, TaskState.FAIL)

            if (threads <= 1) {
                assertTrue(durationMs in 250..350)
            } else {
                assertTrue(durationMs in 50..150)
            }
        }
    }
}